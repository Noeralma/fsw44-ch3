// const date = new Date();

// const previousDay = new Date(date);

// previousDay.setDate(previousDay.getDate() - 18);

// const format = previousDay.toLocaleDateString('en-US', { weekday: 'long' });

// console.log(format);



const age = 25;

const date = new Date();

const year = date.getFullYear() - age;

const birthday = new Date(year, date.getMonth(), date.getDate());

const format = birthday.toLocaleDateString('en-US', { weekday: 'long', year: 'numeric', month: 'long', day: 'numeric' });

console.log(`Tanggal lahir: ${format}`);
