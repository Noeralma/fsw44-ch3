const myText = "I am a string";
const newString = myText.replace("string", "Banana");
console.log(newString);

const myNumber = Math.random();
console.log(myNumber);

const calculation = (radius) => {
	const area = Math.PI * radius**2;
    const fixedArea = area.toFixed(2)
    const finalArea = parseFloat(fixedArea)
    return finalArea
};

const cylinderArea = (radius, height) => {
	return calculation(radius) * height
}

console.log(calculation(7))
console.log(cylinderArea(7, 10))

const random = (number) => {
    return Math.floor(Math.random() * number);
};

const greetings = () => {
    console.log('Hello my name is Alif');
};

const hello = (name = 'Alif') => {
    console.log(`Hello ${name}!`);
};

hello('Nur');
hello();

const add = (a, b) => {
    return a + b;
};

console.log(add(1, 2)); 

const todayDate = () => {
const date = new Date();
const day = String(date.getDate()).padStart(2, '0');
const month = String(date.getMonth() + 1).padStart(2, '0');
const year = date.getFullYear();

return `${day}/${month}/${year}`;
}

console.log(todayDate());

const bmiCalculation = (height, weight) => {
    const bmi = weight / (height**2);
    const bmiFixed = bmi.toFixed(2);
    const finalBmi = parseFloat(bmiFixed);
    return finalBmi
};

console.log('BMI:', bmiCalculation(1.72, 60));

const words = (sentence) => {
    return sentence.charAt(0).toUpperCase();
};

console.log(words('Harry Potter'));

const typeCheck = (word) => {
    return typeof word == 'string' ? 'Yes, it is' : "No, it isn't ";
};

console.log('Is input a string?', typeCheck(12));
console.log('Is input a string?', typeCheck('Hi!'));

const myFuture = (partner, children, location, job) => (`You will be a ${job} in ${location}, and married to ${partner} with ${children} kids.`);


console.log(myFuture('Sarah', 1, 'Bandung', 'Electrical Engineer'));
console.log(myFuture('Safira', 2, 'Cimahi', 'Software Engineer'));
console.log(myFuture('Salma', 3, 'Pekanbaru', 'Househusband'));

const calculateSupply = (myAge, amount) => {
    const maxAge = 100;
    const calculateAmount = 365 * amount * (maxAge - myAge);
    const roundedAmount = Math.round(calculateAmount);
    return (`You will need ${roundedAmount} to last you until the ripe old age of ${maxAge}`);
};

console.log(calculateSupply(23,57.35));
console.log(calculateSupply(30,100));
console.log(calculateSupply(27,34.27));

const operateOnNumbers = (x, y, operation) => operation(x, y);

const subtraction = (a, b) => a - b;
const multiplication = (a, b) => a * b;
const division = (a, b) => a / b;

console.log(operateOnNumbers(4, 5, (g, r) => g + r));
console.log(operateOnNumbers(4, 5, subtraction));
console.log(operateOnNumbers(4, 5, multiplication));
console.log(operateOnNumbers(4, 5, division));